import'../styles/main.scss';
import EditorTemplate from './editor_template.html';
import EmailEditorTemplate from './editor_email_input_template.html';

export interface editorSettings {
  selector: string;
  editorName: string;
  emails: Array<string>;
  placeholder: string;
}

export default class EmailsEditor {
  config: editorSettings;
  emailField: Element;
  emailFiledClass: string;

  emailsListHolder: Element;
  emailsList: string;

  addEmailBtn: Element;
  addEmailBtnClass: string;

  countEmailsBtn: Element
  countEmailsBtnClass: string;

  emailsListing: Element;
  emailsListingClass: string;

  placeholderValue: string;

  persitanceData: Array<string>;

  constructor(config: editorSettings) {
    if (!config) {
      throw "Please provide basic settings: element selector";
      return;
    }

    this.config = config;
    this.persitanceData = [];
    this.initEditor();
  }

  initEditor() {
    this.renderTemplate(); 
    this.getTemplateElements();

    this.multiInput();
  }

  get editorSelector(): Element {
    return document.querySelector(`${this.config.selector}`);
  }

  getElement(elClass: string): Element {
   return this.editorSelector.querySelector(`${elClass}`); 
  }

  renderTemplate() {
    this.editorSelector.innerHTML = EditorTemplate;
    this.editorSelector.innerHTML = this.editorSelector.innerHTML.replace(
      '{board_name}', 
      this.config.editorName
    );
    this.editorSelector.innerHTML = this.editorSelector.innerHTML.replace(
      '{placeholder_value}', 
      this.config.placeholder
    );
  }

  getTemplateElements() {
    this.emailFiledClass =  '.emailField';
    this.addEmailBtnClass = '.addEmail';
    this.countEmailsBtnClass = '.countEmails';
    this.emailsList = '.email-editor__editor--list';
    this.emailsListingClass = '.displayList';

    this.emailField = this.getElement(`${this.emailFiledClass}`);
    this.addEmailBtn = this.getElement(`${this.addEmailBtnClass}`);
    this.countEmailsBtn = this.getElement(`${this.countEmailsBtnClass}`);
    this.emailsListHolder = this.getElement(`${this.emailsList}`);
    this.emailsListing = this.getElement(`${this.emailsListingClass}`)
  }

  validateEmail(email: string): boolean {
    return /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test ( email );
  }

  multiInput() {
    this.emailField.addEventListener('keydown', (e: KeyboardEvent) => this.handleInputValues(e));
    this.emailField.addEventListener('blur', (e: KeyboardEvent) => this.handleInputValues(e));

    this.addEmailBtn.addEventListener('click', (e) => this.generateEmails(e));
    this.countEmailsBtn.addEventListener('click', (e) => this.showValidEmails(e));
  }

  handleInputValues(e: KeyboardEvent) {
    const keycode = e.which || e.keyCode;
    let value = (<HTMLInputElement>event.target).value;
    let emails = [];

    switch (keycode) {
      case 32:
      case 13:
        emails.push(value.split(' '));
        break;
      case 188:
        emails.push(value.split(','));
        break;
    }

    if (e.type === 'blur') {
      emails.push(value.split(/[\s,]+/));
    }

    if (emails.length <= 0) {
      if (value.match(/,/) || value.match(/ /)) {
        emails.push(value.split(/[\s,]+/));
      }
    }

    this.renderEmailBlocks(emails[0]);
  }

  renderEmailBlocks(emails: Array<string>) {
    if (!emails) return;

    emails.forEach((email) => {
      if (email === '') return;

      let emailList = document.createElement('div');
      emailList.innerHTML = EmailEditorTemplate;

      if (!this.validateEmail(email)) {
        emailList.className = 'email-editor__editor--list-invalid';
      } else {
        this.persitanceData.push(email);
      }

      emailList.innerHTML = emailList.innerHTML.replace('{email_address}', email);
      this.emailsListHolder.appendChild(emailList);

      emailList.querySelector('.delete-item').addEventListener('click', (e) => this.removeAddedItem(e, emailList));
    });

    (<HTMLInputElement>this.emailField).value = '';
    (<HTMLInputElement>this.emailField).focus();
  }

  removeAddedItem(e: Event, parent: Element) {
    parent.parentNode.removeChild(parent);
  }

  generateEmails(e: Event) {
    const emails = `name${Math.floor(Math.random() * Math.floor(100))}@domain.com`;
    let list = [];
    list.push(emails);

    this.renderEmailBlocks(list);
  }

  showValidEmails(e: Event) {
    if (this.persitanceData.length <= 0) {
      this.emailsListing.innerHTML = 'Add first some email addresses.'
    }
    const ul = document.createElement('ul');

    this.persitanceData.forEach(data => {
      const list = document.createElement('li');
      list.innerHTML = `<a href="mailto:${data}">${data}</a>`;
      ul.appendChild(list)
    });
    this.emailsListing.innerHTML = '';
    this.emailsListing.appendChild(ul);
  }
}

// uncomment me if in dev mode
// const a = new EmailsEditor(settingsA); //editorSettings global defined in index.html
// const b = new EmailsEditor(settingsB); //editorSettings global defined in index.html

(<any>window).EmailsEditor = EmailsEditor;
