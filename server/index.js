const express = require('express');
const bodyParser = require('body-parser');

const config = require('./config/config');
const api = require('./api');
const middleware = require('./middleware/middleware');

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(middleware.cors);

app.get('/',function(req,res) {
  res.sendFile(__dirname + '/public/index.html');
});
app.use(express.static(__dirname + '/public'))

app.use('/api', api);

app.use((err, req, res, next) => {
  res.status(500).send(err.message);
});

app.listen(config.port, () => {
  console.log('microservice is up on %s port', config.port);
});

module.exports = app;
