const path = require('path');
const assetsServerPort = 6970;

module.exports = {
  env: process.env.NODE_ENV || 'development',
  port: process.env.PORT || 6969
};
