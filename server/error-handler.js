exports.requestErrorHandler = (next, err) => {
  return err ? this.status(err.status || 500).json(err.json()) : next(err);
};
