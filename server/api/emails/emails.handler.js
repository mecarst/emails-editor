const emailsService = require('./emails.service');
const { requestErrorHandler } = require('../../error-handler');
const url = require('url');

exports.getEmails = (req, res, next) => emailsService.getEmails()
  .then(emails => res.set('Content-Type', emails))
  .catch(requestErrorHandler.bind(res, next));

exports.addEmails = (req, res, next) => {
  const emails = req.body.emails;

  return emailsService.addEmails(fonts)
    .then(res => {
      res.set('Content-Type',  'text/plain');
      res.send('Emails added');
    })
    .catch(requestErrorHandler.bind(res, next));
}
