const data = [
  "nick@domain.com",
  "alex@domain.com",
  "james@domain.com",
  "jack@domain.com"
];

exports.getEmails = () => {
  return new Promise((res, rej) => data).catch(e => reject(`Something went wrong: ${e}`));
}

exports.addEmails = (emails) => {
  return new Promise((res, rej) => data.push(emails)).catch(e => reject(`Something went wrong: ${e}`));
}
