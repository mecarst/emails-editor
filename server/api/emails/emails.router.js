const router = require('express').Router();
const emailsHandler = require('./emails.handler');

router
    .get('/emails', emailsHandler.getEmails)
    .post('/emails', emailsHandler.addEmails);

module.exports = router;
