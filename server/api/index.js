const router = require('express').Router();
const emails = require('./emails/emails.router');

router.use(emails);

module.exports = router;
