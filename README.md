## Install the app

- `npm i`

### Runing the app
There are two ways to run the application.

## Development mode
- `npm run serve` - this will start the application as a static webpack application.
Dev mode serves static data also in emails-editor uncomment line 182

# Static Address location
- `http://localhost:8080/`


## Server mode
- `npm run start` || `npm run start:dev`
The server is an expressjs api which serves the public bundle created by webpack.
Also exposes `/api` endpoints, GET and POST for getting and adding new email addresses.
The data it's not stored anyware so on server close will be lost.

In the `public/index.html`  multiple instances of the editor can be made.

# Server Address location
- `http://localhost:6969/`

API
- `GET http://localhost:6969/emails/`
- `POST http://localhost:6969/emails/`


#### The editors needs to be initalized manually
- development mode uncomment line 182 in emails-editor
- server uncomment the initaliziation in `server/public/index.html`


P.S. API calls are not implemented just yet (they are just available)