const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

module.exports = {
  entry: {
    app: [path.join(__dirname, '../app/js/emails-editor.ts')],
    styles: path.join(__dirname, '../app/styles/main.scss')
  },
  output: {
    // library: 'EmailsEditor',
    // libraryTarget: 'var',
    filename: 'js/[name].bundle.js',
    path: path.join(__dirname, '../server/public/'),
    publicPath: './'
  },
  externals: {
    EmailsEditor: {
      commonjs: 'EmailsEditor',
      commonjs2: 'EmailsEditor',
      amd: 'EmailsEditor',
    },
  },
  module: {
    rules: [
      { test: /\.ts?$/, loader: 'babel-loader' },
      { test: /\.html?$/, loader: 'html-loader' },
      { test: /\.js$/, enforce: "pre" },
      { test: /\.txt$/, use: 'raw-loader' },
      { test: /\.json$/, use: ['json-loader'] },
      {
        test: /\.(jpe?g|png|gif|svg)$/,
        loader: 'url-loader?limit=4000&publicPath=./&name=imgs/[name].[ext]'
      }, 
      {
        test: /\.(eot|ttf|woff|woff2)$/,
        loader: 'file-loader?&publicPath=./&name=fonts/[name].[ext]'
      },
      {
         test:/\.(s*)css$/,
         use:['style-loader','css-loader', 'sass-loader']
      }
    ]
  },
  resolve: {
    extensions: [ '.ts', '.js', '.json', '.css', '.scss' ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: 'app/index.html',
      inject: 'body',
      filename: 'index.html'
    }),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.HotModuleReplacementPlugin(), 
    new webpack.NamedModulesPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
          'NODE_ENV': JSON.stringify('development')
      }
    }),
    new MiniCssExtractPlugin({
      filename: '[name].css',
      chunkFilename:'[id].css'
    })
  ],
  devtool: 'cheap-source-map',
  devServer: {
    historyApiFallback: true,
    publicPath: 'http://localhost:8080/'
  }
};
